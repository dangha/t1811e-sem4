package com.example.authanation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthanationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthanationApplication.class, args);
    }

}
