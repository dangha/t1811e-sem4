package com.example.springbootrest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor

public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;

    private BigDecimal price;
    @CreationTimestamp
    private Date createAt;
    @UpdateTimestamp
    private Date updateAt;

    @ManyToOne(fetch = FetchType.LAZY,optional = true)
    //luc nao can thi moiw query tranh truy xuat du lieu khong
    //can thiet
    @JoinColumn(name = "category_id",referencedColumnName = "id")
    private Category productCategory;

    @OneToMany(
            mappedBy = "orderDetailProduct",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<Order_detail> order_details;


}
