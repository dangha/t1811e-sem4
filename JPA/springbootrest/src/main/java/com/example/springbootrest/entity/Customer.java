package com.example.springbootrest.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Data
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private String name;
    private String Address;
    @CreationTimestamp
    private Date createAt;
    @UpdateTimestamp
    private Date updateAt;

    @OneToMany(
            mappedBy = "customerOrder",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<Order> orders;

//    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    @JoinTable(name = "order_detail",
//                joinColumns = @JoinColumn(name = "order_id"),
//                inverseJoinColumns = @JoinColumn(name = "customer_id"))
//    private Collection<Order> orders;
}
