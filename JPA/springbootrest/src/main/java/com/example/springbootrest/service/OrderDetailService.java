package com.example.springbootrest.service;

import com.example.springbootrest.entity.Order_detail;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class OrderDetailService {
    private final OrderDetailService orderDetailService;
    public Order_detail save(Order_detail order_detail){
        return orderDetailService.save(order_detail);
    }

    public List<Order_detail> findAll(){
        return orderDetailService.findAll();
    }

    public void deleteById(Long id){
        orderDetailService.deleteById(id);
    }

    public Optional<Order_detail> findById(Long id){
        return orderDetailService.findById(id);
    }
}
