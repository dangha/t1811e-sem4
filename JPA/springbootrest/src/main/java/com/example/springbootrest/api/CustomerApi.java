package com.example.springbootrest.api;

import com.example.springbootrest.entity.Customer;
import com.example.springbootrest.entity.Order_detail;
import com.example.springbootrest.entity.Product;
import com.example.springbootrest.service.CutomerService;
import com.example.springbootrest.service.OrderDetailService;
import com.example.springbootrest.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/customer")
@RequiredArgsConstructor
@Slf4j
public class CustomerApi {

    private final CutomerService cutomerService;

    @GetMapping
    public List<Object[]> listCustomer(){
        return cutomerService.ListPtoductOfCustomer();
    }
    //Lay danh sach san pham cua khach hang order_detail, product,customer

}
