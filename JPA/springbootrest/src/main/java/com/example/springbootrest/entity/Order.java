package com.example.springbootrest.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Data
@Entity
@Table(name = "Oder")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customerOrder ;

    @OneToMany(
            mappedBy = "orderDetails",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<Order_detail> order_details;

//    @ManyToMany(mappedBy = "orders")
//    private Collection<Customer> customers;
}
