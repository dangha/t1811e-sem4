package com.example.springbootrest.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Data
@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(
            mappedBy = "productCategory",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)

    private Set<Product> products;


//    private Set<Product> products;
//
//    public void setProducts(Set<Product> products){
//        this.products = products;
//        for(Product product:products){
//            product.setCategory(this);
//        }
//    }
    //Lay ra list danh sach cac don hang cua khach hang nao
}
