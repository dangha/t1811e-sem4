package com.example.springbootrest.service;

import com.example.springbootrest.entity.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CutomerService {
    @Autowired
    EntityManager emf;

    public List<Object[]> ListPtoductOfCustomer(){
        Query query = emf.createQuery("SELECT Customer.name, Product .name from Customer, " +
                "Order_detail ,Product where Customer .id = Order.customerOrder.id" +
                " and Order .id = Order_detail .orderDetails.id" +
                " and Product .id = Order_detail .orderDetailProduct.id");
        List<Object[]> list = query.getResultList();
        for(Object[] result  :list){
            System.out.println(result[0]+" "+result[1]);
        }
        return list;
    }
}
