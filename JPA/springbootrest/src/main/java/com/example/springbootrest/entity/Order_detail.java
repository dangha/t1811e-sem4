package com.example.springbootrest.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Order_detail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "order_id",referencedColumnName = "id")
    private Order orderDetails;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product orderDetailProduct;
}
