package todofullstack.demo.service;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import todofullstack.demo.entity.Todo;
import todofullstack.demo.repository.TodoRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TodoService {
    private final TodoRepository todoRepository;

    public List<Todo> findAll(){
        return todoRepository.findAll();
    }
    public Todo save(Todo todo){
        return todoRepository.save(todo);
    }
    public void deleteById(Long id){
        todoRepository.deleteById(id);
    }
    public Optional<Todo> findById(Long id){
        return todoRepository.findById(id);
    }
}
