package todofullstack.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import todofullstack.demo.entity.Todo;

public interface TodoRepository extends JpaRepository<Todo,Long> {

}
