package todofullstack.demo.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import todofullstack.demo.entity.Todo;
import todofullstack.demo.service.TodoService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/todos")
@RequiredArgsConstructor
public class TodoAPI {
    private final TodoService todoService;

    @GetMapping
    public ResponseEntity<List<Todo>> findAll(){
        return ResponseEntity.ok(todoService.findAll());
    }
    @PostMapping
    public ResponseEntity saveAll(@Valid @RequestBody Todo todos){
        return ResponseEntity.ok(todoService.save(todos));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Todo> update(@PathVariable Long id,@Valid @RequestBody Todo todo ){
        Optional<Todo> to = todoService.findById(id);
        if(!to.isPresent()){
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(todoService.save(todo));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Todo> delete(@PathVariable Long id){
        Optional<Todo> todo = todoService.findById(id);
        if(!todo.isPresent()){
            ResponseEntity.badRequest().build();
        }
        todoService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
