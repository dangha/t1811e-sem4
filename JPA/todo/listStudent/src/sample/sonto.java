package sample;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class sonto {
    public TextField txtSTN;
    public TextArea txtKQ;

    public boolean isSoNguyenTo(int x) {
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return x > 1;
    }

    public void submit() {
        try {
            int soTN = Integer.parseInt(txtSTN.getText());
            int i = 1;
            for (i = soTN + 1; ; i++) {
                if (isSoNguyenTo(i)) {
                    break;
                }
            }
            txtKQ.setText("" + i);
        } catch (Exception e) {
//       txtKQ.setText(e.getMessage());
            txtKQ.setText("Invalid number");
        }
    }
}

