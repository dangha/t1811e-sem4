import React, { useEffect, useState } from "react";
import "../assets/styles/todoList.css";
import axios from "axios";

const AddTaskForm = ({ addTask }) => {
    const [value, setValue] = useState("");

    const handleSubmit = e => {
        e.preventDefault();
        value && addTask(value)
        setValue("");
    };

    return (
        <form onSubmit={ handleSubmit }>
            <input
                type="text"
                value={ value }
                placeholder="Enter a title for this task…"
                onChange={ e => setValue(e.target.value) }
            />
            <button type="submit" style={ { fontSize: 20 } }>+</button>
        </form>
    );
}

const ToDoList = () => {

    const [tasks, setTasks] = useState([{
        title: "Like",
        isCompleted: false
    }, {
        title: "Comment",
        isCompleted: false
    }, {
        title: "Subscribe",
        isCompleted: false
    }]);

    function rqGetData() {
        axios
            .get(`http://localhost:9090/api/v1/todos`)
            .then((res) => {
                setTasks(res.data);
            })
            .catch((error) => console.log(error));
    }

    function rqPostData(data) {
        axios
            .post(`http://localhost:9090/api/v1/todos`, JSON.stringify(data), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then((res) => {
                rqGetData();
               
            })
            .catch((error) => console.log(error));
    }

    useEffect(() => {
        rqGetData();
    }, [])

    const addTask = (title) => {
        rqPostData([...tasks, { title }]);
    }

    const toggleTask = index => {
        const newTasks = [...tasks];
        newTasks[index].isCompleted = !newTasks[index].isCompleted;
        setTasks(newTasks);
    };

    const removeTask = id => {
        rqDeleteItem(id);
    };

    function rqDeleteItem(id) {
        axios
            .delete(`http://localhost:9090/api/v1/todos/` + id)
            .then((res) => {
                rqGetData();
            })
            .catch((error) => console.log(error));
    }

    return (
        <div className="todo-list">
            {tasks.map((task, index) => (
                <div className="todo">
                    <span onClick={ () => toggleTask(index) } className={ task.isCompleted ? "todo-text todo-completed" : "todo-text" } style={ { fontSize: 16 } }>
                        { task.title }
                    </span>
                    <button onClick={ () => removeTask(task.id) } style={ { fontSize: 20 } }>X</button>
                </div>
            )) }
            <AddTaskForm addTask={ addTask } />
        </div>
    );
}

export default ToDoList;