package com.example.lombok;

import com.example.lombok.entity.Address;
import com.example.lombok.entity.Library;
import com.example.lombok.entity.LibraryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class LombokApplication implements CommandLineRunner {

    private final LibraryRepository libraryRepository;

    public static void main(String[] args) {
        SpringApplication.run(LombokApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        libraryRepository.save(new Library("Library 1",new Address("street1","zip1")));
        libraryRepository.save(new Library("Library 2",new Address("street2","zip2")));
    }
}
