package com.example.lombok.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @OneToOne(cascade = CascadeType.ALL)//the hien moi quan he giua address va library la 1 1
    @JoinColumn(unique = true)
    private Address address;

    public Library(String name, Address address) {
        this.name = name;
        this.address = address;
       // this.address.setLi((this));

    }
}
