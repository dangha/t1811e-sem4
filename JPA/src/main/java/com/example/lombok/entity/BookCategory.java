package com.example.lombok.entity;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "BookCategory")
@Table(name = "book_category")
public class BookCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    @Column(name = "category_name")
    private String name;
    @OneToMany(
        mappedBy = "bookCategory",
        cascade = CascadeType.ALL,
        fetch = FetchType.LAZY)
    private Set<Book> books;
    public BookCategory(){

    }
    public void setBooks(Set<Book> books){
        this.books = bookCategory;
        bookCategory.getBooks().add(this);
    }
}
