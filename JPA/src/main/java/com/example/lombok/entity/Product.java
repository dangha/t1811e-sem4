package com.example.lombok.entity;

import lombok.*;

//@NoArgsConstructor//k duoc tao contructor ma k co doi so
//@RequiredArgsConstructor//k duoc tao contructor rong
//@AllArgsConstructor//tao contructor co day du doi so

//@Getter
//@Setter
//@ToString
//@EqualsAndHashCode

public class Product {

    @NonNull
    private Integer id;
    private String name;

//    public Product(@NonNull Integer id,String name){
//        this.id = id;
//        this.name = name;
//    }
}
