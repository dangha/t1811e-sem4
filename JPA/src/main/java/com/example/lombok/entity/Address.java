package com.example.lombok.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String street;

    private String zipcode;
    @OneToOne(mappedBy = "address")
    private Library library;


    public Address(String street,String zipcode){
        this.street = street;
        this.zipcode = zipcode;
    }

}
