package aptech.fpt.spring.entity;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {
        User st = (User) o;
        if (st.getName().equals("Bob")) {
            errors.rejectValue("name", null, "Da ton tai.");
        }
    }
}
