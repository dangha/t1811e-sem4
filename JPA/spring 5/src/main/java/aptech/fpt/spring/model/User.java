package aptech.fpt.spring.model;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface User extends JpaRepository<User,Integer> {
    @EntityGraph(attributePaths = "bookCategory")
    List<User> findFirst5ByOrderByNameAsc();

    @Modifying
    @Transactional
    @Query("DELETE FROM User b WHERE b.id =?1")
    void deleteByUserId(int id);

    @Query("DELETE FROM User b WHERE b.id =?1")
    void updateByUserId(int id);
}
