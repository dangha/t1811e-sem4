package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.UserValidator;
import aptech.fpt.spring.model.User;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Optional;

public class UserController {
    private User User;

    public UserController(User User) {
        this.User = User;
    }

    @RequestMapping(path = "/user/create", method = RequestMethod.GET)
    public String createProduct(@ModelAttribute User u) {
        return "user-form";
    }

    @RequestMapping(path = "/user/create", method = RequestMethod.POST)
    public String saveProduct(@Valid User user, BindingResult result) {
        new UserValidator().validate(user, result);
        if (result.hasErrors()) {
            return "user-form";
        }
        User.save(user);
        return "redirect:/user/list";
    }
    @RequestMapping(path = "/user/delete/{id}", method = RequestMethod.POST)
    public String deleteProduct(@PathVariable int id) {
        Optional<User> optionalUser = User.findById(id);
        if (optionalUser.isPresent()) {
            User.delete(optionalUser.get());
            return "redirect:/user/list";
        } else {
            return "not-found";
        }
    }
}

