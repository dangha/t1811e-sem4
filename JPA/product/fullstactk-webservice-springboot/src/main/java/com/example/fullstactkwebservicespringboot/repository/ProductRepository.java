package com.example.fullstactkwebservicespringboot.repository;

import com.example.fullstactkwebservicespringboot.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {

}
