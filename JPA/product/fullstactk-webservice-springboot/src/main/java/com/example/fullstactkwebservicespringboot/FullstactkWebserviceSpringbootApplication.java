package com.example.fullstactkwebservicespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullstactkWebserviceSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(FullstactkWebserviceSpringbootApplication.class, args);
    }

}
