Hi Customer<br>
Thanks for using our service. Please confirm your email address by clicking on the link below
${VERIFICATION_URL}<br>
if you did not sign up please disregard this email.<br>
