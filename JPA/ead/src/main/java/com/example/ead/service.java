package com.example.ead;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class service {
    private final repository repository;

    public service(com.example.ead.repository repository) {
        this.repository = repository;
    }

    public List<user> findAll(){
        return repository.findAll();
    }

    public Optional<user> findById(Long id){
        return repository.findById(id);
    }

    public user save(user user){
        return repository.save(user);
    }

    public void deleteById(Long id){
        repository.deleteById(id);
    }
}
