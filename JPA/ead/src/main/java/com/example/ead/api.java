package com.example.ead;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class api {
    private final service service;

    public api(service todoService) {
        this.service = todoService;
    }

    @GetMapping
    public ResponseEntity<List<user>> findAll(){
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody user user){
        return ResponseEntity.ok(service.save(user));
    }

    @PutMapping("/{id}")
    public ResponseEntity<user> update(@PathVariable Long id,@Valid @RequestBody user user ){
        Optional<user> user1 = service.findById(id);
        if(!user1.isPresent()){
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(service.save(user));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<user> delete(@PathVariable Long id){
        Optional<user> user = service.findById(id);
        if(!user.isPresent()){
            ResponseEntity.badRequest().build();
        }
        service.deleteById(id);
        return ResponseEntity.ok().build();
    }
}