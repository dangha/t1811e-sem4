<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 9/10/2020
  Time: 8:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:useBean id="e" class="model.employee" scope="request"/>
<form method="post" action="Servlet">
    <tr>
        <td>Full Name: </td>
        <td><input type="text" name="name" value='${e.fullName}'></td>
    </tr>
    <tr>
        <td>Address: </td>
        <td><input type="text" name="txtLastName" value='${e.address}'></td>
    </tr>
    <tr>
        <td>Position: </td>
        <td><input type="text" name="txtBirthDate"  value='${e.position}'></td>
    </tr>
    <tr>
        <td>Department: </td>
        <td><input type="text" name="txtHireDate" value='${e.department}'></td>
    </tr>
    <button type="submit" class="btn btn-success">Save</button>
    <button type="reset" class="btn btn-success">Reset</button>
</form>
</body>
</html>
