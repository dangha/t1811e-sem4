<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 9/10/2020
  Time: 8:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Position</th>
        <th>Department</th>
    </tr>
    </thead>
    <tbody>
    <c:set var="list" value="${requestScope.list}" scope="request"/>
    <c:forEach var="list" items = "${list}">
        <tr>
            <td>
                <c:out value="${list.id}"/>
            </td>
            <td>
                <c:out value="${list.FullName}"/>
            </td>
            <td>
                <c:out value="${list.address}"/>
            </td>
            <td>
                <c:out value="${list.Posstion}"/>
            </td>
            <td>
                <c:out value="${list.Department}"/>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
