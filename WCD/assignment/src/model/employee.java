package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class employee {
    private String ID;
    private String FullName;
    private Date birthDay;
    private String address;
    private String position;
    private String department;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public employee() {
    }

    private static final String tblName ="employes";

    private static Connection conn = null;

    private static Statement stmt = null;

    private static PreparedStatement pstm = null;

    protected Connection createConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        return connection;
    }
    private static final String INSERT_USERS_SQL = "INSERT INTO employes"+"(NAME,EMAIL,COUNTRY)VALUES"+
            "(?,?,?);";
    private static final String SELECT_ALL_USERS = "SELECT * FROM USERS";
    public List<employee> selectAllUsers()throws SQLException,ClassNotFoundException{
        List<employee> employees = new ArrayList<>();
        Connection connection = createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("fullname");
            String address = rs.getString("address");
            String possition = rs.getString("possition");
            String department = rs.getString("department");
            employees.add(new employee(id,name,address,possition,department));
        }
        return employees;
    }
}
