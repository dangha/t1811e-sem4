package controller;

import model.employee;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ID = request.getParameter("ID");
        String fullname = request.getParameter("FullName");
        String Address = request.getParameter("Address");
        employee bean = new employee();
        bean.setID(ID);
        bean.setFullName(fullname);
        bean.setAddress(Address);

        try{
            employee.createConnection();
            HttpSession session = request.getSession();
            session.setAttribute("bean",bean);
            response.sendRedirect("list.jsp");

        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private void listUser(HttpServletRequest req, HttpServletResponse resp) throws SQLException,IOException,ServletException,ClassNotFoundException{
        List<employee> list = employee.;
        req.setAttribute("list",list);
        RequestDispatcher dispatcher = req.getRequestDispatcher("list.jsp");
        dispatcher.forward(req,resp);
    }
}
