<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 9/2/2020
  Time: 8:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css/menu_style.css" type="text/css" rel="stylesheet" />
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <title></title>
</head>
<body>
<div class="header">
    <br>
    <h1 align="center">Employees Manager</h1>
    <div class="menu bubplastic horizontal orange">
        <ul>
            <li class="highlight"><span class="menu_r"><a href="login.jsp"><span class="menu_ar">login</span></a></span></li>
            <li><span class="menu_r"><a href="employeeManager.jsp"><span class="menu_ar">Employee Manager</span></a></span></li>
            <li><span class="menu_r"><a href="processEmployee"><span class="menu_ar">Add New Employee</span></a></span></li>
            <li><span class="menu_r"><a href="searchEmployee.jsp"><span class="menu_ar">Search Employee</span></a></span></li>
            <li><span class="menu_r"><a href="logout.jsp"><span class="menu_ar">logout</span></a></span></li>
        </ul>
        <br class="clearit" />
    </div>
</div>
<div class="content">
    <br><BR>
<%--    <%--%>
<%--        String type = request.getParameter("type");--%>
<%--        String msg = request.getParameter("msg");--%>
<%--        if (type.equals("error")) {--%>
<%--    %>--%>
<%--    <h1 class="error"><%=msg%></h1>--%>
<%--    <%--%>
<%--    } else if (type.equals("info")) {--%>
<%--    %>--%>
<%--    <h1><%=msg%></h1>--%>
<%--    <%--%>
<%--    } else if (type.equals("warning")) {--%>
<%--    %>--%>
<%--    <h1><%=msg%></h1>--%>
<%--    <%--%>
<%--        }--%>
<%--    %>--%>
    <c:choose>
        <c:when test="${param.type eq 'error'}">
            <h1 class="error">${param.msg}</h1>
        </c:when>
        <c:when test="${param.type eq 'infor'}">
            <h1 class="error">${param.msg}</h1>
        </c:when>
        <c:when test="${param.type eq 'warning'}">
            <h1 class="error">${param.msg}</h1>
        </c:when>
    </c:choose>
</div>
</body>
</html>

