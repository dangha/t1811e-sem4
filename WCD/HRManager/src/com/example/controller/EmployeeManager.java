package com.example.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class EmployeeManager extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException,IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
        }
        com.example.bol.EmployeeBO ebo = new com.example.bol.EmployeeBO();
        request.setAttribute("EmpBO", ebo);
        String value = request.getParameter("txtValue");
        String option = request.getParameter("ddlSearch");
        if (value == null || option == null) {
            RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp");
            reqdis.forward(request, response);
        } else {
            RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp?option=" + option + "&value=" + value);
            reqdis.forward(request, response);
        }
    }
}
