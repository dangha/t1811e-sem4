package com.example.controller;

import com.example.Model.Employee;
import com.example.bol.EmployeeBO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ProcessEmployee extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        EmployeeBO ebo = new com.example.bol.EmployeeBO();
        Employee e;
        String action = request.getParameter("action");
        String id = request.getParameter("id");
        String fName = request.getParameter("txtFirstName");
        String lName = request.getParameter("txtLastName");
        if (lName == null || fName == null) {
            if (action == null || action.equals("add")) {
                e = new Employee();
            } else {
                e = ebo.getByID(Integer.parseInt(id));
            }
            request.setAttribute("e", e);
            RequestDispatcher reqdis = request.getRequestDispatcher("processEmployee.jsp");
            reqdis.forward(request, response);
            return;
        }
        if (action != null && action.equals("delete")) {
            try {
                e = ebo.getByID(Integer.parseInt(id));
                if (e == null) {
                    response.sendRedirect("InformationPage.jsp?type=error&msg=No have this EmployeeID");
                } else {
                    if (ebo.delete(e) > 0) {
                        response.sendRedirect("InformationPage.jsp?type=info&msg=This EmployeeID deleted");
                    } else {
                        response.sendRedirect("InformationPage.jsp?type=warning&msg=This EmployeeID can't delete");
                    }
                }
            } catch (NumberFormatException ex) {
                response.sendRedirect("InformationPage.jsp?type=error&msg=This ins't EmployeeID");
                return;
            }
        } else {
            e = new com.example.Model.Employee();
            com.example.ConvertData convert = new com.example.ConvertData();
            e.setFirstName(fName);
            e.setLastName(lName);
            e.setBirthDate(convert.string2date(request.getParameter("txtBirthDate")));
            e.setHireDate(convert.string2date(request.getParameter("txtHireDate")));
            e.setAddress(request.getParameter("txtAddress"));
            e.setCity(request.getParameter("txtCity"));
            e.setCountry(request.getParameter("txtCountry"));
            e.setEmail(request.getParameter("txtEmail"));
            e.setHomePhone(request.getParameter("txtHomePhone"));
            e.setMobile(request.getParameter("txtMobile"));
            e.setNote(request.getParameter("txtNote"));
            if (action.equals("edit")) {
                try {
                    e.setEmployeeID(Integer.parseInt(id));
                    if (ebo.edit(e) > 0) {
                        response.sendRedirect("InformationPage.jsp?type=info&msg=This EmployeeID updated");
                    } else {
                        response.sendRedirect("InformationPage.jsp?type=warning&msg=This EmployeeID can't update");
                    }
                } catch (NumberFormatException ex) {
                    response.sendRedirect("InformationPage.jsp?type=error&msg=This ins't EmployeeID");
                    return;
                }
            } else {
                if (ebo.add(e) > 0) {
                    response.sendRedirect("InformationPage.jsp?type=info&msg=This EmployeeID inserted to database");
                } else {
                    response.sendRedirect("InformationPage.jsp?type=warning&msg=This EmployeeID can't insert to database");
                }
            }
        }
    }
}
