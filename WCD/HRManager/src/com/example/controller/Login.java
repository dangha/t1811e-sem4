package com.example.controller;

import com.example.Model.User;
import com.example.bol.UserBO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Login extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException,IOException {
        String userName = request.getParameter("txtUserName");
        String userPassword = request.getParameter("txtPassword");
        com.example.ValidData valid = new com.example.ValidData();
        if (userName == null) {
            goPage(request, response, "login.jsp?error=UserName");
        } else if (userPassword == null || !valid.isPassword(userPassword)) {
            goPage(request, response, "login.jsp?error=Password");
        } else {
            User u = new User();
            UserBO ubo = new UserBO();
            u.setUserName(userName);
            u.setUserPassword(userPassword);
            if (ubo.authorization(u)) {
                HttpSession session = request.getSession();
                session.setAttribute("username", u.getUserName());
                goPage(request, response, "EmployeeManager");
            } else {
                goPage(request, response, "login.jsp?error=Error");
            }
        }
    }

    private void goPage(HttpServletRequest request, HttpServletResponse response, String link)
    throws ServletException,IOException{
        RequestDispatcher regdis = request.getRequestDispatcher(link);
        regdis.forward(request,response);
    }
}
