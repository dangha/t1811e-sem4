package com.example.entity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
    protected int id;
    protected String name;
    protected String email;
    protected String country;

    public User(){

    }

    public User(String name,String email,String country){
        super();
        this.name = name;
        this.email = email;
        this.country = country;
    }
    public User(int id,String name,String email,String country){
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
//    private static Connection conn = null;
//    public static void createConnection() throws SQLException, ClassNotFoundException {
//        Class.forName("com.mysql.cj.jdbc.Driver");
//        conn =
//                DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sinhvien","root",
//                        "");
//    }
//    public static void u() throws SQLException {
//        String query = "Select name,email from users";
//        pstm = conn.prepareStatement(query);
//        ResultSet resultSet = pstm.executeQuery();
//        while (resultSet.next()){
//            System.out.println("name: "+resultSet.getString("name"));
//        }
//    }
//    public static void main(String[] args) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        if(connection!=null){
//            u();
//        }
//    }

}

