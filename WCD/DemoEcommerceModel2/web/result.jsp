<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 8/28/2020
  Time: 8:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>List Product</title>
</head>
<body>
<h1>List Product!</h1>
<a href="search.jsp">Search</a>
<jsp:useBean id="finder" class="com.wpsj.model.ProductFinderBean" scope="request"/>

<table>
    <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Description</td>
    </tr>
    <c:forEach items="${finder.products}" var="product">
        <tr>
            <td><c:out value="${product.id}"/></td>
            <td><c:out value="${product.name}"/></td>
            <td><c:out value="${product.desc}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
