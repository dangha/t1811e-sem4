package com.wpsj.da;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static Connection connection;
    public Connection getConnection() throws ClassNotFoundException, SQLException{
        if(connection==null){
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            connection =
                    DriverManager.getConnection("jdbc:derby://localhost:808/sinhvien","sa","sa");

        }return connection;
    }
}
