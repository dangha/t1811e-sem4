package com.wpsj.controller;
import com.wpsj.model.ProductFinderBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProductFinder extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String keyword = request.getParameter("name");
        if(keyword==null||keyword.trim().isEmpty()){
            response.sendRedirect("search.jsp?msg=Enter keyword pls!");
            return;
        }
        ProductFinderBean finder = new ProductFinderBean();
        finder.setKeyword(keyword);
        request.setAttribute("finder",finder);

        RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
        rd.forward(request,response);
    }
}
