package com.example.onetwomany.respository;

import com.example.onetwomany.entity.Book;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;//ACID

import java.util.List;

@Transactional(readOnly = true)
public interface BookRepository extends JpaRepository<Book,Integer> {
    @EntityGraph(attributePaths = "bookCategory")
    List<Book> findFirs5tByOrderByNameAsc();

    @Modifying
    @Transactional
    @Query("delete from Book b where b.bookCategory.id=?1")
    void deleteByBookCategoryId(int categoryId);

}
