package com.example.onetwomany.respository;

import com.example.onetwomany.entity.BookCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookCategoryRepository extends JpaRepository<BookCategory,Integer> {

}
