package com.example.onetwomany;

import com.example.onetwomany.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnetwomanyApplication implements CommandLineRunner {

    private final BookService bookService;

    public OnetwomanyApplication(BookService bookService) {
        this.bookService = bookService;
    }

    public static void main(String[] args) {
        SpringApplication.run(OnetwomanyApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        bookService.create();
        bookService.read();
        bookService.delete();
    }
}
