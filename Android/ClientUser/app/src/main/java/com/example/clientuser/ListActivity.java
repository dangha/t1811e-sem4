package com.example.clientuser;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientuser.adapter.UserAdapter;
import com.example.clientuser.model.user;
import com.example.clientuser.network.APIManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListActivity extends AppCompatActivity {
    RecyclerView rvListUsers;
    List<user> listData;
    UserAdapter userAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_users);

        //b1 data source
        getData();
        //b2 adapter
        listData = new ArrayList<>();
        userAdapter = new UserAdapter(this,listData);

        //b3 layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        //b4 recycleView
        rvListUsers = findViewById(R.id.rvListUsers);
        rvListUsers.setLayoutManager(layoutManager);
        rvListUsers.setAdapter(userAdapter);
    }

    private void getData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service = retrofit.create(APIManager.class);
        service.getListData().enqueue(new Callback<List<user>>() {
            @Override
            public void onResponse(Call<List<user>> call, Response<List<user>> response) {
                if(response.body() != null){
                    listData = response.body();
                    userAdapter.reloadData(listData);
                }
            }

            @Override
            public void onFailure(Call<List<user>> call, Throwable t) {
                Toast.makeText(ListActivity.this,"Fail",Toast.LENGTH_LONG).show();
            }
        });
    }
}
