package com.example.clientuser;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.clientuser.model.user;

public class UserActivity extends AppCompatActivity {
    private EditText edUsername;
    private EditText edPassword;
    private EditText edEmail;
    private EditText edFullName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_user);

        user user = (user) getIntent().getSerializableExtra("user");
        edUsername = findViewById(R.id.edUser);
        edPassword = findViewById(R.id.edPassword);
        edEmail = findViewById(R.id.edEmail);
        edFullName = findViewById(R.id.edName);

        edUsername.setText(user.getUsername());
        edPassword.setText(user.getPassword());
        edEmail.setText(user.getEmail());
        edFullName.setText(user.getName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}

