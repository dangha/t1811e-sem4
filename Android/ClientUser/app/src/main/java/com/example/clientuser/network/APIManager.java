package com.example.clientuser.network;

import com.example.clientuser.model.user;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIManager {
    String SERVER_URL = "https://5fd782299dd0db0017ee95e0.mockapi.io/api/v1/";

    @GET("users")
    Call<user> getData();

    @GET("users")
    Call<List<user>> getListData();
}
