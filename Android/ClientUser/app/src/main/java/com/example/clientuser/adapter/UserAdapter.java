package com.example.clientuser.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientuser.R;
import com.example.clientuser.model.user;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<user> list;

    public UserAdapter(Activity activity,List<user> list){
        this.activity = activity;
        this.list = list;
    }

    public void reloadData(List<user> list){
        this.list = null;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.item_user,parent,false);
        UsersHolder holder = new UsersHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UsersHolder hd = (UsersHolder) holder;
        user model = list.get(position);
        hd.tvName.setText(model.getName());
        hd.tvEmail.setText(model.getEmail());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UsersHolder extends RecyclerView.ViewHolder{
        TextView tvName,tvEmail;

        public UsersHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvFullName);
            tvEmail = itemView.findViewById(R.id.tvMail);
        }
    }

}
