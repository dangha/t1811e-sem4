package com.example.layoutdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}

//linear_layout: sx tcac view theo hai huong vertical dung; horizoller ngang
//1.kich thuoc
//match_parent hoac fill_parent: set theo kich thuoc cha
//wrap_content: theo noi dung ma no chua
//height weight
//2.can chinh
//gravity:can chinh noi dung center,..
//