package com.example.assignment.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.assignment.R;
import com.example.assignment.databse.DB;

public class ListActivity extends AppCompatActivity {
    private DB db;
    private Cursor c;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        db = new DB(this);
        ListView lvUser = (ListView) findViewById(R.id.lvUser);

        c = db.getAllUser();

        adapter = new SimpleCursorAdapter(this, R.layout.item, c, new String[]{
                DB.ID, DB.NAME, DB.GENDER}, new int[]{
                R.id.tvId, R.id.tvName, R.id.tvGender
        }, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvUser.setAdapter(adapter);

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) adapter.getItem(position);
                int _id = cursor.getInt(cursor.getColumnIndex(DB.ID));
                String name = cursor.getString(cursor.getColumnIndex(DB.NAME));
                String gender = cursor.getString(cursor.getColumnIndex(DB.GENDER));
                String des = cursor.getString(cursor.getColumnIndex(DB.DES));

                Intent intent = new Intent(ListActivity.this, MainActivity.class);
                intent.putExtra(DB.ID, _id);
                intent.putExtra(DB.NAME, name);
                intent.putExtra(DB.GENDER, gender);
                intent.putExtra(DB.DES, des);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        c = db.getAllUser();
        adapter.changeCursor(c);
        adapter.notifyDataSetChanged();
        db.close();
    }
}
