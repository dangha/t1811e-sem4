package com.example.assignment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.assignment.R;
import com.example.assignment.databse.DB;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private DB db;
    private int _id;
    private EditText edName;
    private EditText edGender;
    private EditText edDes;
    private EditText edEmail;

    private Button btAdd;
    private CheckBox checkBox;
    private Spinner spinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DB(this);

        initView();

        Intent intent = getIntent();
        _id = intent.getIntExtra(DB.ID,0);
        String name = intent.getStringExtra(DB.NAME);
        String gender = intent.getStringExtra(DB.GENDER);
        String email = intent.getStringExtra(DB.EMAIL);
        String des = intent.getStringExtra(DB.DES);

        edName.setText(name);
        edGender.setText(gender);
        edEmail.setText(email);
        edDes.setText(des);
    }

    private void initView() {
        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.edEmail);
        edDes = (EditText) findViewById(R.id.edDes);
        btAdd = (Button) findViewById(R.id.btAdd);
        checkBox = (CheckBox) findViewById(R.id.ck);
        btAdd.setOnClickListener(this);

        String[] genders = {"Male","Female","Unknow"};
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,genders);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == btAdd){
            onRegister();
        }
    }

    private void onRegister() {
        if(edName.getText().toString().isEmpty() && edEmail.getText().toString().isEmpty()
        && edDes.getText().toString().isEmpty()){
            Toast.makeText(this,"Khong de trong",Toast.LENGTH_LONG).show();
            return;
        }
        if(!checkBox.isChecked()){
            Toast.makeText(this,"Please agree",Toast.LENGTH_LONG).show();
            return;
        }
        String isAdd = db.addUser(edName.getText().toString(),spinner.getSelectedItem().toString(),
                edDes.getText().toString());
        Toast.makeText(this,isAdd,Toast.LENGTH_LONG).show();

        Intent intent = new Intent(MainActivity.this,ListActivity.class);
        startActivity(intent);
    }
}

