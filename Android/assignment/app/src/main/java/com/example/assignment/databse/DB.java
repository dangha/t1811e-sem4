package com.example.assignment.databse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DB extends SQLiteOpenHelper {
    public static final String DB_Name = "USER";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "TBL_USER";
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String DES = "des";

    public DB( Context context) {
        super(context, DB_Name, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + "(" +
                ID + "INTEGER PRIMARY KEY, " +
                NAME + "TEXT," +
                EMAIL + "TEXT," +
                GENDER + "TEXT, " +
                DES + "TEXT )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    public String addUser(String user,String gender,String des){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, user);
        cv.put(GENDER,gender);
        cv.put(DES, des);
        long isSuccess = db.insert(TABLE_NAME, null, cv);
        if( isSuccess == -1){
            return "Add Fail";
        }else{
            return "Add success";
        }
    }


    public Cursor getAllUser(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME;
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
}

