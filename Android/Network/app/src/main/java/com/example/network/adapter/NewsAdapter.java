package com.example.network.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.network.R;
import com.example.network.activity.ListNewsActivity;
import com.example.network.interfaces.NewsOnClicks;
import com.example.network.model.Item;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<Item> listData;
    private NewsOnClicks iOnClick;

    public NewsAdapter(Activity activity, List<Item> listData, NewsOnClicks iOnClick) {
        this.activity = activity;
        this.listData = listData;
        this.iOnClick = iOnClick;
    }

    public NewsAdapter(Activity activity, List<Item> listData) {
        this.activity = activity;
        this.listData = listData;
    }

    public void setiOnClick(NewsOnClicks iOnClick) {
        this.iOnClick = iOnClick;
    }

    public void reloadData(List<Item> listData){
        this.listData = listData;
        this.notifyDataSetChanged();
    }
    public class NewsHolder extends RecyclerView.ViewHolder{
        TextView tvData,tvTitle,tvContent;
        ImageView ivCover;


        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            tvData = itemView.findViewById(R.id.tvDate);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            ivCover = itemView.findViewById(R.id.ivCover);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iOnClick.onClickItem(getAdapterPosition());
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
