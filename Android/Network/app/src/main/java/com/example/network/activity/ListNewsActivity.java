package com.example.network.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.network.R;
import com.example.network.adapter.NewsAdapter;
import com.example.network.interfaces.NewsOnClicks;
import com.example.network.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ListNewsActivity extends AppCompatActivity {

    private List<Item> listDatas = new ArrayList<>();
    private NewsAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_news);
        //b1: data source
        getData();

        //b2 Adapter
        adapter = new NewsAdapter(this, listDatas);
        //b3 layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        //b4 RecyclerView
        RecyclerView rvNews = findViewById(R.id.rvNews);
        rvNews.setLayoutManager(layoutManager);
        rvNews.setAdapter(adapter);
        adapter.setiOnClick(new NewsOnClicks() {
            @Override
            public void onClickItem(int position) {
                Item model = listDatas.get(position);
                Intent intent = new Intent(ListNewsActivity.this,DetailActivity.class);
                intent.putExtra("URL",model.getContent().getUrl());
                startActivity(intent);
            }
        });
    }

    private void getData() {
    }
}
