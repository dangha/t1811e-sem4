package com.example.network.interfaces;

public interface NewsOnClicks {
    void onClickItem(int position);
}
