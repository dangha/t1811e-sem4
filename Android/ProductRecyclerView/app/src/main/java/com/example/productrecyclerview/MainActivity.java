package com.example.productrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Product> listProduct = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //b1 data source
        initData();
        //B2 Adapter
        ProductAdapter adapter = new ProductAdapter(this,listProduct);
        //b3 layout manager
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);
        //b4 recycleView
        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData() {
        listProduct.add(new Product("Product 1","500.000",R.drawable.p1));
        listProduct.add(new Product("Product 2","500.000",R.drawable.p2));
        listProduct.add(new Product("Product 3","500.000",R.drawable.p3));
        listProduct.add(new Product("Product 4","500.000",R.drawable.p4));
        listProduct.add(new Product("Product 5","500.000",R.drawable.p5));
    }
}