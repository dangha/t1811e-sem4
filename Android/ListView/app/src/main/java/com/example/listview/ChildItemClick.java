package com.example.listview;

public interface ChildItemClick {
    void onItemChildClick(int position);
}
