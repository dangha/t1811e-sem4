package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//Khoi tao các tuonf tác người dùng reference đến các
//lớp còn lại
public class MainActivity extends AppCompatActivity  {

    private ListView lvContact;
    private List<ContactModel> listContact = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
//        initView();

        lvContact = (ListView) findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContact,this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = listContact.get(position);
                Toast.makeText(MainActivity.this,contactModel.getName(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        ContactModel contactModel = new ContactModel("Nguyen a","0123456",R.drawable.ic_user_a);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen b","1234211",R.drawable.ic_user_b);
        listContact.add(contactModel);

        contactModel = new ContactModel("Nguyen c","1234211",R.drawable.ic_user_c);
        listContact.add(contactModel);
        contactModel = new ContactModel("Tran b","1234211",R.drawable.ic_user_d);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen d","1234211",R.drawable.ic_user_b);
        listContact.add(contactModel);
        contactModel = new ContactModel("Nguyen Van b","1234211",R.drawable.ic_user_b);
        listContact.add(contactModel);
        contactModel = new ContactModel("Tran Thi b","1234211",R.drawable.ic_user_b);
        listContact.add(contactModel);
    }
}